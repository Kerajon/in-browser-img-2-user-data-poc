import { createWorker } from 'tesseract.js';
import {Observable} from 'rxjs';

export function recognize$(img, lang: string = 'eng'): Observable<any> {
  return new Observable(subscriber => {
    const worker = createWorker({
      workerPath: 'assets/worker.min.js',
      corePath: 'assets/tesseract-core.wasm.js',  // TODO: WASM detection and path fallback to ASM.js
      // langPath  TODO: download common lang paths  TODO: point lang asset to local server
      logger: ({ status, progress }) => subscriber.next({ status, progress })
    });
    (async () => {
      try {
        await worker.load();
        await worker.loadLanguage(lang);
        await worker.initialize(lang);
        const { data } = await worker.recognize(img);
        await worker.terminate();
        subscriber.next({ status: 'text recognized', progress: 1, data });
        subscriber.complete();
      } catch (err) {
        subscriber.error(err);
      }
    })();
  });
}
