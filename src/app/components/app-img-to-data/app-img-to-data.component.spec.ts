import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppImgToDataComponent } from './app-img-to-data.component';

describe('AppImgToDataComponent', () => {
  let component: AppImgToDataComponent;
  let fixture: ComponentFixture<AppImgToDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppImgToDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppImgToDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
