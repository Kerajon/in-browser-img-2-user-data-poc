import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {recognize$} from '@utils/ocr';
import {ImageService} from '../../image.service';
import {Router} from '@angular/router';
import {OcrService} from '../../ocr.service';
interface IntrinsicState {
  name: string;
  titleName?: string;
  subTitleName?: string;
  progress?: number;
}
@Component({
  selector: 'app-app-img-to-data',
  templateUrl: './app-img-to-data.component.html',
  styleUrls: ['./app-img-to-data.component.scss']
})
export class AppImgToDataComponent implements OnInit, OnDestroy {
  states: { [stateName: string]: IntrinsicState } = {
    input: {
      name: 'input',
      titleName: 'Read data from picture',
      subTitleName: 'Feed it with picture',
    },
    pending: {
      name: 'pending',
      titleName: '...processing',
      subTitleName: '',
      progress: 0
    }
  };
  state: IntrinsicState;
  imgBlob;
  @ViewChild('inputImgFileSystem', { static: true })inputImgFileSystem;

  constructor(
    private imageService: ImageService,
    private ocrService: OcrService,
    private router: Router,
  ) { }

  readPicture() {
    this.ocrService.recognize$(this.imgBlob).subscribe(
      (next) => {
        this.setState('pending', {
          subTitleName: next.status,
          progress: next.progress * 100
        });

        if (next.status === 'text recognized') {
          this.setState('input');
          this.imageService.load = true; // Yeah yeah - i know ;)
          this.router.navigate(['/schema-creator']);
        }
      },
      console.error
    );
  }
  onChooseImage() {
    this.setState('pending', { titleName: 'Choose image' });
  }
  onFileSelected(event) {
    this.imgBlob = event.target.files[0];
    this.setState('input', { titleName: 'Click READ DATA', subTitleName: 'and wait for magic to happen' });
  }
  setState(stateName: string, stateExtend: { [key: string]: any } = {}): void {
    this.state = this.states[stateName];
    Object.assign(this.state, stateExtend);
  }
  ngOnInit(): void {
    this.setState('input');
    this.inputImgFileSystem.nativeElement.addEventListener('change', this.onFileSelected.bind(this), false);
  }
  ngOnDestroy(): void {
    this.inputImgFileSystem.nativeElement.removeEventListener('change', this.onFileSelected.bind(this));
  }

}
