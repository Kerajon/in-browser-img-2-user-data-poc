import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppImgToSchemaComponent } from './app-img-to-schema.component';

describe('AppImgToSchemaComponent', () => {
  let component: AppImgToSchemaComponent;
  let fixture: ComponentFixture<AppImgToSchemaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppImgToSchemaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppImgToSchemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
