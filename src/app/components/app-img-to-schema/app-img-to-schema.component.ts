import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ImageService} from '../../image.service';
import {Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';
import {OcrService} from '../../ocr.service';
import Konva from 'konva/lib/Core';
import { Image as KonvaImage } from 'konva/lib/shapes/Image';
import { Rect as KonvaRect } from 'konva/lib/shapes/Rect';

@Component({
  selector: 'app-app-img-to-schema',
  templateUrl: './app-img-to-schema.component.html',
  styleUrls: ['./app-img-to-schema.component.scss']
})
export class AppImgToSchemaComponent implements OnInit, OnDestroy {
  url;
  image;
  ratio: number;
  @ViewChild('canvasContainer', {static: false}) canvasContainer: ElementRef;
  @ViewChild('canvas', {static: true}) canvas: ElementRef;
  private ctx: CanvasRenderingContext2D;
  subscription: Subscription;
  constructor(
    private imageService: ImageService,
    private ocrService: OcrService,
  ) { }

  private loadImage(url: string): void {
    this.image = new Image();
    this.image.onload = () => {
      this.drawImageScaled();
    };
    this.image.src = url;
  }
  private drawImageScaled() {
    const img = this.image;
    const width = this.canvasContainer.nativeElement.clientWidth;
    const height = this.canvasContainer.nativeElement.clientHeight;

    const hRatio = width / img.width;
    const vRatio = height / img.height;
    this.ratio = Math.min(hRatio, vRatio);
    if (this.ratio > 1) {
      this.ratio = 1;
    }

    this.canvas.nativeElement.width = img.width * this.ratio;
    this.canvas.nativeElement.height = img.height * this.ratio;

    this.ctx.clearRect(0, 0, width, height);
    this.ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, img.width * this.ratio, img.height * this.ratio);
  }
  drawBBox(bbox: { x0: number, x1: number, y0: number, y1: number }) {
    if (bbox) {
      this.drawImageScaled();

      this.ctx.beginPath();
      this.ctx.moveTo(bbox.x0 * this.ratio, bbox.y0 * this.ratio);
      this.ctx.lineTo(bbox.x1 * this.ratio, bbox.y0 * this.ratio);
      this.ctx.lineTo(bbox.x1 * this.ratio, bbox.y1 * this.ratio);
      this.ctx.lineTo(bbox.x0 * this.ratio, bbox.y1 * this.ratio);
      this.ctx.closePath();
      this.ctx.strokeStyle = '#bada55';
      this.ctx.lineWidth = 2;
      this.ctx.stroke();
    }
  }
  onSizeUp() {
    this.canvas.nativeElement.width = this.canvas.nativeElement.width * 1.1;
    this.canvas.nativeElement.height = this.canvas.nativeElement.height * 1.1;
    this.drawImageScaled();
  }
  onSizeDown() {
    this.canvas.nativeElement.width = this.canvas.nativeElement.width * 0.9;
    this.canvas.nativeElement.height = this.canvas.nativeElement.height * 0.9;
    this.drawImageScaled();
  }
  ngOnInit() {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.subscription = this.imageService.getBlobImageUrl$()
      .pipe(
        filter(url => !!url)
      )
      .subscribe(
        url => {
          this.ocrService.getRecognized$().subscribe(
            (result) => {
              console.log(result);
              const bbox = (result as any).lines[6].bbox;
              console.log(bbox);
              loadImage(url as string, (img) => {
                const stage = createStage({
                  container: 'canvas_container',
                  width: img.width,
                  height: img.height
                });
                const layer = createLayer();
                const image = createImage({
                  image: img,
                  x: 0,
                  y: 0,
                  width: img.width,
                  height: img.height
                });
                layer.add(image);

                (result as any).words.forEach((word) => {
                  layer.add(
                    createWordBox(
                      word,
                      convertTesseractBboxToKonvaDimentions(word.bbox),
                      layer
                    )
                  );
                });
                stage.add(layer);
              });
            }
          );
        }
      );
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
function createStage(config) {
  return new Konva.Stage(config);
}
function createLayer() {
  return new Konva.Layer();
}
function loadImage(url: string, cb: (img) => void) {
  const image = new Image();
  image.onload = () => {
    cb(image);
  };
  image.src = url;
}
function createImage(config) {
  return new KonvaImage(config);
}
function convertTesseractBboxToKonvaDimentions({x0, y0, x1, y1}) {
  return {
    x: x0,
    y: y0,
    width: x1 - x0,
    height: y1 - y0
  };
}
function createWordBox(tesseractWord, dimentions, layer) {
  let selected = false;
  const wordBox = new KonvaRect(dimentions);
  wordBox.on('mouseover', function() {
    if (!selected) {
      this.fill('red');
      this.opacity(0.5);
      layer.draw();
    }
  });
  wordBox.on('mouseout', function() {
    if (!selected) {
      this.fill(null);
      this.stroke(null);
      this.opacity(1);
      layer.draw();
    }
  });
  wordBox.on('click', function() {
    selected = !selected;
    if (selected) {
      this.stroke('red');
      this.fill('yellow');
      this.opacity(0.3);
      console.log(`Selected "${tesseractWord.text}"`, tesseractWord);
    } else {
      this.fill(null);
      this.stroke(null);
      this.opacity(1);
    }
    layer.draw();
  });
  return wordBox;
}
