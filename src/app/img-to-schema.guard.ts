import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {ImageService} from './image.service';

@Injectable({
  providedIn: 'root'
})
export class ImgToSchemaGuard implements CanActivate {
  constructor(
    private imageService: ImageService,
    private router: Router
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return !!this.imageService.load ? true : this.router.createUrlTree(['/']);
  }
}
