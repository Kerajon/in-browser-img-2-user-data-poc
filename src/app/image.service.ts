import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import {combineLatest, debounceTime, filter, map, switchMap, tap} from 'rxjs/operators';
import {NgxOpenCVService} from 'ngx-opencv';

declare const cv: any;

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  load: boolean;
  blobImage;
  imageUrl: string;
  constructor(
    private ngxOpenCVService: NgxOpenCVService

  ) { }
  static blobImageToUrl$(img) {
    return new Observable(subscriber => {
      const reader = new FileReader();
      reader.readAsDataURL(img);
      reader.onload = (_) => {
        const imageUrl = reader.result as string;
        subscriber.next(imageUrl);
        subscriber.complete();
      };
    });
  }
  static canvasToImage(canvas) {
    const image = new Image();
    image.src = canvas.toDataURL('image/png');
    return image;
  }

  saveImage(blobImage) {
    this.blobImage = blobImage;
  }

  toOpenCVFormat$(url) {
    const img = new Image();
    return new Observable(subscriber => {
      img.onload = async () => {
        // tslint:disable-next-line:variable-name
        const canvasImg: HTMLCanvasElement = document.createElement('canvas');
        canvasImg.width = img.width;
        canvasImg.height = img.height;
        const canvasCtx = canvasImg.getContext('2d');
        canvasCtx.drawImage(img, 0, 0);
        subscriber.next(canvasImg);
        subscriber.complete();
      };
      img.src = url;
    });
  }

  getBlobImageUrl$() {
    return new Observable(subscriber => {
      subscriber.next(this.imageUrl);
      subscriber.complete();
    });
  }

  threshold$(img) {
    return this.ngxOpenCVService.cvState.pipe(
      filter(state => state.ready),
      debounceTime(10),
      switchMap(() => ImageService.blobImageToUrl$(img)),
      debounceTime(10),
      switchMap(url => this.toOpenCVFormat$(url)),
      debounceTime(10),
      map(opencvFormatImg => {
        const dst = cv.imread(opencvFormatImg);
        // convert the image to grayscale, blur it, and find edges in the image
        cv.cvtColor(dst, dst, cv.COLOR_RGBA2GRAY, 0);
        // find contours
        cv.threshold(dst, dst, 127, 255, cv.THRESH_BINARY);
        const canvasProcessedImg = document.createElement('canvas');
        cv.imshow(canvasProcessedImg, dst);
        const processedImg = ImageService.canvasToImage(canvasProcessedImg);
        this.saveImage(processedImg);
        this.imageUrl = canvasProcessedImg.toDataURL('image/png');
        return processedImg;
      })
    );
  }
}
