import { Injectable } from '@angular/core';
import {recognize$} from '@utils/ocr';
import {BehaviorSubject} from 'rxjs';
import {filter, switchMap, tap} from 'rxjs/operators';
import {ImageService} from './image.service';

@Injectable({
  providedIn: 'root'
})
export class OcrService {
  private ocrResult$ = new BehaviorSubject({});
  constructor(
    private imgService: ImageService,
  ) { }

  recognize$(img) {
    return this.imgService.threshold$(img).pipe(
      switchMap(cvimg => recognize$(cvimg)),
      tap(data => {
        if (data.status === 'text recognized') {
          this.ocrResult$.next(data.data);
        }
      })
    );
  }

  getRecognized$() {
    return this.ocrResult$;
  }
}
