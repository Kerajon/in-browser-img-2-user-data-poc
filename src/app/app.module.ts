import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgxDocumentScannerModule} from 'ngx-document-scanner';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppImgToDataComponent } from './components/app-img-to-data/app-img-to-data.component';
import {MatButtonModule, MatCardModule, MatProgressBarModule} from '@angular/material';
import { AppImgToSchemaComponent } from './components/app-img-to-schema/app-img-to-schema.component';

@NgModule({
  declarations: [
    AppComponent,
    AppImgToDataComponent,
    AppImgToSchemaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatProgressBarModule,
    NgxDocumentScannerModule.forRoot({
      openCVDirPath: './assets'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
