import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppImgToDataComponent } from './components/app-img-to-data/app-img-to-data.component';
import { AppImgToSchemaComponent } from './components/app-img-to-schema/app-img-to-schema.component';
import {ImgToSchemaGuard} from './img-to-schema.guard';


const routes: Routes = [
  {
    path: '',
    component: AppImgToDataComponent
  },
  {
    path: 'schema-creator',
    component: AppImgToSchemaComponent,
    canActivate: [ImgToSchemaGuard]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
